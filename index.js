'use strict'

const sync = require('./lib/sync')

switch (process.argv[2]) {
  case 'initial':
    sync.initialSync()
    break
  case 'remain':
    sync.remainSync()
    break
  case 'image':
    sync.imagesSync()
    break
  case 'replace':
    sync.replaceDb()
    break
  default:
    sync.initialSync()
    break
}
