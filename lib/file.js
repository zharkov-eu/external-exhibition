'use strict'

const fs = require('fs')
const sharp = require('sharp')
const uuid = require('uuid/v4')
const config = require('../config')

module.exports.storeImage = (img) => {
  return new Promise((resolve, reject) => {
    let promiseArray = []
    let image = {}

    let filenames = []
    for (var i = 0; i < 3; i++) {
      filenames.push((`${uuid()}.jpg`).replace(/-/g, ''))
    }

    if (!img.Content) resolve(null)

    fs.writeFile(`${config.system.imagePath}/${filenames[0]}`, img.Content, 'base64', (err) => {
      if (err) return reject(new Error(`file.storeImage -> ${err}`))
      promiseArray.push(sharp(`${config.system.imagePath}/${filenames[0]}`).resize(249, 160)
        .toFile(`${config.system.imagePath}/${filenames[1]}`))
      promiseArray.push(sharp(`${config.system.imagePath}/${filenames[0]}`).resize(171, 110)
        .toFile(`${config.system.imagePath}/${filenames[2]}`))
      Promise.all(promiseArray)
        .then(() => {
          image[img.ViewId] = {450: filenames[0], 160: filenames[1], 110: filenames[2]}
          resolve(image)
        }, (err) => {
          console.error('storeImage -> sharp resize problem -> ' + err)
          image[img.ViewId] = {450: filenames[0]}
          resolve(image)
        })
    })
  })
}

module.exports.removeImage = (filenames) => {
  return new Promise((resolve, reject) => {
    let counter = Object.keys(filenames).length

    for (let i = 0, keys = Object.keys(filenames); i < keys.length; i++) {
      fs.unlink(`${config.system.imagePath}/${filenames[keys[i]]}`, (err) => {
        if (err) return reject(new Error(`file.removeImage -> ${err}`))
        if (--counter === 0) resolve(null)
      })
    }
  })
}
