'use strict'

module.exports = (ids, data, field) => {
  let hash = {}

  let insertArray = []
  let removeArray = []

  for (let i = 0; i < ids.length; i++) {
    if (hash[ids[i][field]]) removeArray.push(hash[ids[i][field]]._id)
    hash[ids[i][field]] = ids[i]
  }

  for (let i = 0; i < data.length; i++) {
    if (!hash[data[i][field]]) {
      insertArray.push(data[i])
    } else if (hash[data[i][field]].Hash !== data[i].Hash) {
      if (hash[data[i][field]].Images) data[i].Images = hash[data[i][field]].Images
      removeArray.push(hash[data[i][field]]._id)
      insertArray.push(data[i])
    }
    delete (hash[data[i][field]])
  }

  for (let item in hash) {
    removeArray.push(hash[item]._id)
  }

  return ([insertArray, removeArray])
}
