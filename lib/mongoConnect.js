'use strict'

const config = require('../config')
const MongoClient = require('mongodb').MongoClient

const url = config.system.dbstring

module.exports = MongoClient.connect(url)
