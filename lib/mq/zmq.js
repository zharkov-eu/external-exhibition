'use strict'

const zmq = require('zeromq')
const port = 'tcp://127.0.0.1:5556'

function timeout (ms) {
  return new Promise(resolve => setTimeout(resolve, ms))
}

function connect (port) {
  let socket = zmq.socket('req')
  socket.identity = 'UltimaSyncClient' + process.pid
  socket.connect(port)
  return socket
}

async function ping (port) {
  let socket = connect(port)
  let pong = false
  socket.on('message', (data) => {
    if (data === 'pong') pong = true
  })
  socket.send('ping')
  await timeout(500)
  return pong
}

function pub () {
  let socket = zmq.socket('pub')
  socket.identity = 'UltimaSyncPublisher' + process.pid
}

exports.connect = connect
exports.ping = ping
exports.pub = pub
