'use strict'

const request = require('request')
const config = require('../config.json')

module.exports = (params) => {
  return new Promise((resolve, reject) => {
    let aliveCheck = setTimeout(() => {
      reject(new Error('reqpromise timeout error: ' + options.url))
    }, 60000)

    const options = {
      url: `${config.system.baseURL}${params.method}?format=json${params.query}`,
      headers: { 'Cookie': global.sessionCookie }
    }

    request(options, (err, res) => {
      if (err) return reject(new Error(err))
      try {
        let data = JSON.parse(res.body)
        clearTimeout(aliveCheck)
        resolve(data)
      } catch (err) {
        return reject(new Error(err))
      }
    })
  })
}
