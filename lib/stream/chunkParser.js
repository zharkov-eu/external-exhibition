'use strict'

function ChunkParser () {
  this.cache = ''
}

ChunkParser.prototype.parse = function (chunk) {
  let self = this

  chunk = self.cache + chunk

  let items = []
  let pos = -1
  let child = 0
  let start = 0
  let last = 0;

  (function newChild () {
    if (chunk.indexOf('{', ++pos) !== -1) {
      let openMatch = chunk.indexOf('{', pos)

      if (chunk.indexOf('}', pos) === -1) {
        self.cache = chunk.substring(last ? ++last : 0, chunk.length)

        if (!child) {
          if (self.cache[0] === ',') self.cache = self.cache.substring(1)
          if (self.cache[self.cache.length - 1] === ']' && self.cache[self.cache.length - 2] === '}') self.cache = self.cache.substring(0, self.cache.length - 1)
        }
        try {
          JSON.parse(self.cache)
          items.push(self.cache)
          self.cache = ''
          return 0
        } catch (err) {
          return 0
        }
      }
      let closeMatch = chunk.indexOf('}', pos)

      if (openMatch < closeMatch) {
        pos = openMatch
        if (++child === 1) {
          start = openMatch
        }
        return newChild()
      } else {
        pos = closeMatch
        if (--child === 0) {
          last = pos
          items.push(chunk.substring(start, pos + 1))
        }
        return newChild()
      }
    } else {
      (function checkEnds () {
        if (chunk.indexOf('}', pos) !== -1) {
          pos = chunk.indexOf('}', pos) + 1
          return checkEnds(--child)
        } else {
          return 0
        }
      })()

      self.cache = chunk.substring(last ? ++last : 0, chunk.length)

      if (!child) {
        if (self.cache[0] === ',') self.cache = self.cache.substring(1)
        if (self.cache[self.cache.length - 1] === ']') self.cache = self.cache.substring(0, self.cache.length - 1)
      }
      try {
        JSON.parse(self.cache)
        items.push(self.cache)
        self.cache = ''
        return 0
      } catch (err) {
        return 0
      }
    }
  })()

  return items
}

module.exports = ChunkParser
