'use strict'

const StringDecoder = require('string_decoder').StringDecoder

module.exports = (stream, action, properties, func) => {
  const BUFFER_LENGTH = properties.BUFFER_LENGTH || 1000

  switch (action) {
    case 'return':

      const ReturnStream = require('./parseAndReturn')
      const returnStream = new ReturnStream(properties)

      return new Promise((resolve, reject) => {
        const decoder = new StringDecoder('utf8')
        let parseBuffer = ''
        let result = []

        stream.on('error', (err) => {
          reject(new Error(`stream -> ${err}`))
        })

        stream.on('chunk', (data) => {
          parseBuffer += decoder.write(data)

          if (parseBuffer.length > BUFFER_LENGTH) {
            let parsed = returnStream.parseAndReturn(parseBuffer)
            if (func) parsed = func.handle(parsed)
            result = result.concat(parsed)
            parseBuffer = ''
          }
        })

        stream.on('end', () => {
          if (parseBuffer) {
            let parsed = returnStream.parseAndReturn(parseBuffer)
            if (func) parsed = func.handle(parsed)
            result = result.concat(parsed)
            parseBuffer = ''
          }

          resolve(result)
        })
      })

    case 'insert':

      let promiseArray = []

      const InsertStream = require('./parseAndInsert')
      const insertStream = new InsertStream(properties, promiseArray)

      return new Promise((resolve, reject) => {
        const decoder = new StringDecoder('utf8')
        let parseBuffer = ''

        stream.on('error', (err) => {
          reject(new Error(`stream -> ${err}`))
        })

        stream.on('chunk', (data) => {
          parseBuffer += decoder.write(data)

          if (parseBuffer.length > BUFFER_LENGTH) {
            insertStream.parseAndInsert(parseBuffer, func)
            parseBuffer = ''
          }
        })

        stream.on('end', () => {
          if (parseBuffer) {
            insertStream.parseAndInsert(parseBuffer, func)
            parseBuffer = ''
          }

          Promise.all(promiseArray).then(() => {
            resolve()
          })
        })
      })
  }
}
