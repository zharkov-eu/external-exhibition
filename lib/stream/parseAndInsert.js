'use strict'

const ChunkParser = require('./chunkParser')
const transliterate = require('../transliterate')

function InsertStream (properties, promiseArray) {
  this.db = properties.db
  this.collection = properties.collection
  this.hash = properties.hash
  this.promiseArray = promiseArray
  this.parser = new ChunkParser()
}

InsertStream.prototype.parseAndInsert = function (string, func) {
  let parsed = this.parser.parse(string)

  parsed = parsed.map(function (item) {
    let hash = require('crypto').createHash('sha1').update(item).digest('base64')
    item = JSON.parse(item)
    item.Hash = hash
    let uri = transliterate(item.Name).replace(/ /g, '_').toLowerCase()
    uri = uri.replace(/[^a-zA-Z0-9-_]/g, '')
    item.UrlName = item.UrlName || uri.split('_').filter(el => { if (el && el !== '-') return el }).join('_')
    return item
  })

  if (parsed.length === 0) return
  if (func) parsed = func.handle(parsed)

  this.promiseArray.push(this.db.collection(this.collection).insertMany(parsed))

  return parsed
}

module.exports = InsertStream
