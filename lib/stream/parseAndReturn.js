'use strict'

const ChunkParser = require('./chunkParser')
const transliterate = require('../transliterate')

function ReturnStream (properties) {
  this.hash = properties.hashed
  this.parser = new ChunkParser()
}

ReturnStream.prototype.parseAndReturn = function (string) {
  let parsed = this.parser.parse(string)

  if (!this.hash) {
    return parsed.map((item) => {
      return JSON.parse(item)
    })
  } else {
    return parsed.map((item) => {
      let hash = require('crypto').createHash('sha1').update(item).digest('base64')
      item = JSON.parse(item)
      item.Hash = hash
      let uri = transliterate(item.Name).replace(/ /g, '_').toLowerCase()
      uri = uri.replace(/[^a-zA-Z0-9-_]/g, '')
      item.UrlName = item.UrlName || uri.split('_').filter(el => { if (el && el !== '-') return el }).join('_')
      return item
    })
  }
}

module.exports = ReturnStream
