'use strict'

const EventEmmiter = require('events')
const request = require('request')
const config = require('../../config')

class ReqStream extends EventEmmiter {}

module.exports = (params) => {
  const reqStream = new ReqStream()

  const options = {
    url: `${config.system.baseURL}${params.method}?format=json${params.query}`,
    headers: { 'Cookie': global.sessionCookie }
  }

  let alive = true

  let aliveCheck = setInterval(() => {
    if (!alive) reqStream.emit('error', new Error('datachunk timeout error: ' + options.url))
    alive = false
  }, 1000)

  let req = request.get(options)

  req.on('error', (err) => {
    reqStream.emit('error', err)
  })

  req.on('response', (res) => {
    if (res.statusCode !== 200) reqStream.emit('error', res.req._header)
  })

  req.on('data', (data) => {
    alive = true
    reqStream.emit('chunk', data)
  })

  req.on('end', () => {
    clearInterval(aliveCheck)
    reqStream.emit('end')
  })

  return reqStream
}
