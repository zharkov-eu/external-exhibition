'use strict'

/**
 * Проверяет/создает начальный набор коллекций
 * @param {*} db - MongoDB соединение
 */
async function createCollections (db) {
  try {
    let collections = await db.collections()
    let exists = {}
    let createArray = []

    for (let i = 0; i < collections.length; i++) {
      exists[collections[i].s.name] = 1
    }

    if (!exists.items) createArray.push(db.createCollection('items'))
    if (!exists.categories) createArray.push(db.createCollection('categories'))
    if (!exists.contacts) createArray.push(db.createCollection('contacts'))
    if (!exists.customers) createArray.push(db.createCollection('customers'))
    if (!exists.news) createArray.push(db.createCollection('news'))
    if (!exists.pages) createArray.push(db.createCollection('pages'))
    if (!exists.system) createArray.push(db.createCollection('system'))

    return Promise.all(createArray)
  } catch (error) {
    throw new Error(`checkCollection -> ${error}`)
  }
}

module.exports = createCollections
