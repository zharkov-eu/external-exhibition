'use strict'

/**
 * Проверяет индекс на поле, возвращает true если есть и false если не имеется
 * @param {*} collection - коллекция
 * @param {*} index - индекс
 */
async function checkIndex (collection, index) {
  const exists = await collection.indexExists(index.name)
  if (exists) return true
  return false
}

/**
 * Создает указанный индекс для коллекции
 * @param {*} collection - коллекция
 * @param {*} index - индекс
 */
async function addIndex (collection, index) {
  return collection.createIndex(index.main, index.options)
}

/**
 * Проверяет/создает индексы для указанной коллекции
 * @param {*} collection - коллекция
 * @param {*} index - индекс
 */
async function createIndexes (collection, indexes) {
  try {
    const promiseArray = []
    for (let i = 0; i < indexes.length; i++) {
      if (!await checkIndex(collection, indexes[i])) {
        promiseArray.push(addIndex(collection, indexes[i]))
      }
    }
    return Promise.all(promiseArray)
  } catch (error) {
    throw new Error(`lib -> sync -> checkIndexes -> ${error}`)
  }
}

module.exports = createIndexes
