'use strict'

var Promise = require('bluebird')
const fs = Promise.promisifyAll(require('fs'), {suffix: 'Async'})
const path = require('path')
const config = require('../../config')
const mongoConnect = require('../mongoConnect')
const checkCollection = require('./checkCollection')
const updateRemains = require('./updateRemains')
const updateImages = require('./updateImages')
const Api = require('../api')
const ItemController = require('../../controller/item')
const CategoryController = require('../../controller/category')
const StoreController = require('../../controller/store')
const HardcodeController = require('../../hardcode/controller')

/**
 * Обертка для выполняемых фунций синхронизации, инициализирует
 * соединение с mongodb, првоеряет наличие коллекций
 * @param {*} func - функция для вызова, в параметр передано соединение с БД
 */
async function wrapper (func) {
  try {
    const db = await mongoConnect
    await Promise.all([Api.initialConnect(), checkCollection(db)])
    await func(db)
  } catch (error) {
    console.error(`${new Date()}: sync -> index -> wrapper -> ${error}`)
    process.exit(-1)
  }
}

async function initialSync (db) {
  const promiseArray = [ItemController(db), CategoryController(db), StoreController(db), HardcodeController(db)]
  console.time('initialSyncTime')

  Promise.all(promiseArray)
    .then((data) => {
      return updateRemains(db, data[0], data[2])
    })
    .then(() => {
      db.close()
      console.log(`${new Date()}: sync -> index -> initialSync -> finished`)
      console.timeEnd('initialSyncTime')
    })
    .catch((err) => {
      console.error(`${new Date()}: sync -> index -> initialSync -> ${err}`)
      process.exit(1)
    })
}

function remainSync (db) {
  console.time('remainSyncTime')
  const itemMap = db.collection('items').find({}, {'ProductId': 1}).toArray()
  const remainMap = StoreController(db)

  Promise.all([itemMap, remainMap])
    .then((data) => {
      return updateRemains(db, data[0], data[1])
    })
    .then(() => {
      db.close()
      console.log(`${new Date()}: sync -> index -> remainSync -> finished`)
      console.timeEnd('remainSyncTime')
      process.exit(0)
    })
    .catch((err) => {
      console.error(`${new Date()}: sync -> index -> remainSync -> ${err}`)
      process.exit(1)
    })
}

function replaceDb (db) {
  db.collection('customers').find({}).toArray()
    .then((customers) => {
      if (customers.length) fs.writeFileSync(path.join(config.system.hardcodePath, '/customers.json'), JSON.stringify(customers))
      return db.collection('items').find({}, {'ProductId': 1, 'Images': 1}).toArray()
    })
    .then((it) => {
      let promiseArray = []
      for (var i = it.length - 1; i >= 0; i--) {
        if (!it[i].Images) continue
        for (var key in it[i].Images) {
          for (var res in it[i].Images[key]) {
            promiseArray.push(fs.unlinkAsync(config.system.imagePath + '/' + it[i].Images[key][res]))
          }
        }
      }
      return Promise.all(promiseArray)
    })
    .then(() => {
      return db.dropDatabase()
    })
    .then(() => {
      db.close()
      console.log(`${new Date()}: sync -> index -> replaceDb -> finished`)
      process.exit(0)
    })
    .catch((err) => {
      console.error(`${new Date()}: sync -> index -> replaceDb -> ${err}`)
      process.exit(1)
    })
}

module.exports.initialSync = () => {
  return wrapper(initialSync)
}

module.exports.remainSync = () => {
  return wrapper(remainSync)
}

module.exports.imagesSync = () => {
  return wrapper(imagesSync)
}

module.exports.replaceDb = () => {
  return wrapper(replaceDb)
}
