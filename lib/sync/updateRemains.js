'use strict'

/**
 * Обновляет остатки по элементам
 * @param {*} db - объект базы данных
 * @param {*} itemMap - карта элементов
 * @param {*} remainMap - карта остатков
 */
async function updateRemains (db, itemMap, remainMap) {
  let promiseArray
  let keys = []
  if (Array.isArray(itemMap)) {
    for (let i = 0; i < itemMap.length; i++) {
      keys.push(itemMap[i].ProductId)
    }
  } else {
    keys = Object.keys(itemMap)
  }

  for (let i = 0; i < keys.length; i++) {
    promiseArray.push(db.collection('items').updateOne({ProductId: parseInt(keys[i])}, {$set: {Remains: remainMap[keys[i]]}}))
  }
  return Promise.all(promiseArray)
}

module.exports = updateRemains
