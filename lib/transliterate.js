'use strict'

/**
 * Транслитерация по стандарту
 * @param {String} text - Строка для транслитерации
 * @param {Boolean} [engToRus] - С английского на русский?
 * @returns {String} Транслитерированная строка
 */
function transliterate (text, engToRus) {
  if (!text) return ''
  const rus = 'щ   ш  ч  ц  ю  я  ё  ж  ъ  ы  э  а б в г д е з и й к л м н о п р с т у ф х ь'.split(/ +/g)
  const eng = "shh sh ch cz yu ya yo zh `` y' e` a b v g d e z i j k l m n o p r s t u f x `".split(/ +/g)

  for (let i = 0; i < rus.length; i++) {
    text = text.split(engToRus ? eng[i] : rus[i]).join(engToRus ? rus[i] : eng[i])
    text = text.split(engToRus ? eng[i].toUpperCase() : rus[i].toUpperCase()).join(engToRus ? rus[i].toUpperCase() : eng[i].toUpperCase())
  }
  return text
}

module.exports = transliterate
