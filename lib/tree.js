'use strict'

class Queue {
  constructor () {
    this._oldestIndex = 1
    this._newestIndex = 1
    this._storage = {}
  }

  size () {
    return this._newestIndex - this._oldestIndex
  }

  enqueue (data) {
    this._storage[this._newestIndex] = data
    this._newestIndex++
  }

  dequeue () {
    let oldestIndex = this._oldestIndex
    let newestIndex = this._newestIndex
    let deletedData
    if (oldestIndex !== newestIndex) {
      deletedData = this._storage[oldestIndex]
      this._storage[oldestIndex]
      delete this._storage[oldestIndex]
      this._oldestIndex++
      return deletedData
    }
  }
}

class Tree {
  constructor (element) {
    let node = new Node(element)
    this._root = node
  }

  traverseDF (callback) {
    (function recurse (currentNode) {
      for (let i = 0, length = currentNode._children.length; i < length; i++) {
        recurse(currentNode._children[i])
      }
      callback(currentNode)
    })(this._root)
  }

  traverseBF (callback) {
    let queue = new Queue()
    queue.enqueue(this._root)
    let currentTree = queue.dequeue()

    while (currentTree) {
      for (let i = 0, length = currentTree._children.length; i < length; i++) {
        queue.enqueue(currentTree._children[i])
      }

      callback(currentTree)
      currentTree = queue.dequeue
    }
  }

  contains (callback, traversal) {
    traversal.call(this, callback)
  }

  add (element, toElement, traversal) {
    let child = new Node(element)
    let parent = null
    let callback = (node) => {
      if (node.id === toElement) {
        parent = node
      }
    }
    this.contains(callback, traversal)
    if (parent) {
      parent._children.push(child)
      child.parent = parent
    } else {
      throw new Error('Cannot add node to a non-existent parent.')
    }
  }
}

class Node {
  constructor (element) {
    this.id = element.id
    this.name = element.name
    this.hash = element.hash
    this._parent = null
    this._children = []
  }
}

module.exports = Tree
